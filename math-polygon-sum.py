import math

def polysum(n, s):
    '''
    This function takes the number
    
    Expected input: the number of sides `n` of a *regular polygon*,
    and the length `s` of each side

    Output: sum of the area plus the square of the perimeter,
    of a polygon of `n` sides of length `s`;
    this sum is rounded to 4 decimal places
    '''
    
    area_regpoly = (0.25 * n * (s**2) ) / math.tan( math.pi / n )
    peri_regpoly = n * s

    return round((area_regpoly + (peri_regpoly**2)), 4)
