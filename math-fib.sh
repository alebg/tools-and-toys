#/usr/bin/env bash

function fib {
    fib_numA=0
    fib_numB=1

    if [ $1 -ge 1 ]; then
        for n in $(eval echo {1..$1})
        do
            echo -n "$fib_numA "
            fib_numN=$((fib_numA + fib_numB))
            fib_numA=$fib_numB
            fib_numB=$fib_numN
        done
    else
        echo "Error: please enter a positive, non-zero number"
    fi
    echo ""
}
