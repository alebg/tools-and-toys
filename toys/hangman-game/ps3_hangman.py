# Hangman game
#

# -----------------------------------
# Helper code
# You don't need to understand this helper code,
# but you will have to know how to use the functions
# (so be sure to read the docstrings!)

import random
import string

WORDLIST_FILENAME = "words.txt"

def loadWords():
    """
    Returns a list of valid words. Words are strings of lowercase letters.
    
    Depending on the size of the word list, this function may
    take a while to finish.
    """
    print("Loading word list from file...")
    # inFile: file
    inFile = open(WORDLIST_FILENAME, 'r')
    # line: string
    line = inFile.readline()
    # wordlist: list of strings
    wordlist = line.split()
    print("  ", len(wordlist), "words loaded.")
    return wordlist

def chooseWord(wordlist):
    """
    wordlist (list): list of words (strings)

    Returns a word from wordlist at random
    """
    return random.choice(wordlist)

# Load the list of words into the variable wordlist
# so that it can be accessed from anywhere in the program
wordlist = loadWords()

# end of helper code
# -----------------------------------

def isWordGuessed(secretWord, lettersGuessed):
    '''
    secretWord: string, the word the user is guessing
    lettersGuessed: list, what letters have been guessed so far
    returns: boolean, True if all the letters of secretWord are in lettersGuessed;
      False otherwise
    '''
    # FILL IN YOUR CODE HERE...
    secretWord_list = []
    for l in secretWord:
        secretWord_list.append(l)
    secretWord_list = list(set(secretWord_list))
    secretWord_list.sort()
    lettersGuessed.sort()
    return secretWord_list == lettersGuessed  


def getGuessedWord(secretWord, lettersGuessed):
    '''
    secretWord: string, the word the user is guessing
    lettersGuessed: list, what letters have been guessed so far
    returns: string, comprised of letters and underscores that represents
      what letters in secretWord have been guessed so far.
    '''
    # FILL IN YOUR CODE HERE...
    GuessSoFar = []
    lettersGuessed_test = sorted(list(set(lettersGuessed)))

    for i in range(len(secretWord)):
        swl_in_lg = False
        for l in lettersGuessed_test:
            if secretWord[i] == l:    
                GuessSoFar.append(l)
                swl_in_lg = True
                break
        if swl_in_lg == False:
            GuessSoFar.append("_ ")
    GuessSoFar = "".join(GuessSoFar)
    return(GuessSoFar)


def getAvailableLetters(lettersGuessed):
    '''
    lettersGuessed: list, what letters have been guessed so far
    returns: string, comprised of letters that represents what letters have not
      yet been guessed.
    '''
    # FILL IN YOUR CODE HERE...
    all_letters = list(string.ascii_lowercase)
    lettersGuessed_test = sorted(list(set(lettersGuessed)))

    for l in lettersGuessed_test:
        all_letters.remove(l)

    all_letters = "".join(all_letters)
    return all_letters   


def hangman(secretWord):
    '''
    secretWord: string, the secret word to guess.

    Starts up an interactive game of Hangman.
    * At the start of the game, let the user know how many 
      letters the secretWord contains.
    * Ask the user to supply one guess (i.e. letter) per round.
    * The user should receive feedback immediately after each guess 
      about whether their guess appears in the computers word.
    * After each round, you should also display to the user the 
      partially guessed word so far, as well as letters that the 
      user has not yet guessed.

    Follows the other limitations detailed in the problem write-up.
    '''
    # FILL IN YOUR CODE HERE...
    
    # Game setup
    attempt = 1
    game_round = 0
    all_l = list(string.ascii_lowercase)
    lettersGuessed = []
    lettersNotGuessed = []
    print("Welcome to Hangman!\nThe secret word contains", len(secretWord), "letters.\nYou have 8 attempts to guess the word.\n")   
    
    # Loop to repeat rounds
    while attempt < 9:
        # Setup
        game_round += 1
        user_l = ""
        print("======\nROUND", game_round, "\nYou have", 9 - attempt, "attempts left\n======\n")
        print(getGuessedWord(secretWord, lettersGuessed))
        print("\nThe available letters are:", " ".join(list(getAvailableLetters(lettersGuessed + lettersNotGuessed))), "\n")

        # Loop until the user inputs a single letter
        while True:
            user_l = input("Please enter a single letter, without any space: ").lower()  # lowercase the user input
            print("")
            if user_l in all_l:
                if user_l in lettersNotGuessed + lettersGuessed:
                    print("Please enter a letter you have not entered before.")
                elif user_l not in lettersNotGuessed + lettersGuessed:
                    break            

        # Evaluate if the user input is in the secret word
        if user_l not in secretWord:
            lettersNotGuessed.append(user_l)
            attempt += 1
            if attempt > 8:
                print("You are out of attempts.\nGAME OVER!\n")
                print("The secret word was:", secretWord, "\n")
                break
        elif user_l in secretWord:
            lettersGuessed.append(user_l)

        # Evaluate if user guessed the word 
        if isWordGuessed(secretWord, lettersGuessed) == True:
            print("\n", getGuessedWord(secretWord, lettersGuessed), "\n")
            print("Congratulations, you correctly guessed the word!\nYOU WIN!\nAnd you still had", 9 - attempt, "attempts left!")
            break


# When you've completed your hangman function, uncomment these two lines
# and run this file to test! (hint: you might want to pick your own
# secretWord while you're testing)

# Test:
#secretWord = "something"

# Real game: 
secretWord = chooseWord(wordlist).lower()

hangman(secretWord)
