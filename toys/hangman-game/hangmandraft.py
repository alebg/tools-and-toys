def isWordGuessed(secretWord, lettersGuessed):
    '''
    secretWord: string, the word the user is guessing
    lettersGuessed: list, what letters have been guessed so far
    returns: boolean, True if all the letters of secretWord are in lettersGuessed;
      False otherwise
    '''
    secretWord_list = []
    lettersGuessed_test = lettersGuessed
    for l in secretWord:
        secretWord_list.append(l)
    secretWord_list = list(set(secretWord_list))
    secretWord_list.sort()
    lettersGuessed_test.sort()
    return secretWord_list == lettersGuessed_test

# Test of isWordGuessed()
#print(isWordGuessed("apple", ["a", "p", "l", "e"]))


def getGuessedWord(secretWord, lettersGuessed):
    '''
    secretWord: string, the word the user is guessing
    lettersGuessed: list, what letters have been guessed so far
    returns: string, comprised of letters and underscores that represents
      what letters in secretWord have been guessed so far.
    '''
    GuessSoFar = []
    lettersGuessed_test = sorted(list(set(lettersGuessed)))

    for i in range(len(secretWord)):
        swl_in_lg = False
        for l in lettersGuessed_test:
            if secretWord[i] == l:    
                GuessSoFar.append(l)
                swl_in_lg = True
                break
        if swl_in_lg == False:
            GuessSoFar.append("_ ")
    GuessSoFar = "".join(GuessSoFar)
    return(GuessSoFar)

# Test of getGuessedWord
#print(getGuessedWord("apple", ["a", "i", "u", "l", "z", "e"]))

import string

def getAvailableLetters(lettersGuessed):
    '''
    lettersGuessed: list, what letters have been guessed so far
    returns: string, comprised of letters that represents what letters have not
      yet been guessed.
    '''
    all_letters = list(string.ascii_lowercase)
    lettersGuessed_test = sorted(list(set(lettersGuessed)))

    for l in lettersGuessed_test:
        all_letters.remove(l)

    all_letters = "".join(all_letters)
    return all_letters

# Test of getAvailableLetters()
print(getAvailableLetters(["a", "p", "l", "e", "g", "z", "k", "m", "l", "g", "n"]))



