low = 0
high = 100
guess = (low + high) // 2

print("Please think of a number between 0 and 100!")

while True:
    print(low, high)
    print("Is your secret number " + str(guess) + "?")
    usr_input = input("Enter 'h' to indicate the guess is too high. Enter 'l' to indicate the guess is too low. Enter 'c' to indicate I guessed correctly. ")
    if usr_input == 'h':
        high = guess
        guess = (low + high) // 2
    elif usr_input == 'l':
        low = guess
        guess = (low + high) // 2
    elif usr_input == 'c':
        print("Game over.  Your secret number was: " + str(guess))
        break
    else:
        print("Sorry, I did not understand your input.")
        
