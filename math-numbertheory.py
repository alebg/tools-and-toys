'''
Implementation of different elements of Number Theory
    many of these come from HTPI ch. 7 (3rd ed.)
'''

def fib(x):
    ''' Recursive implementation of the Fibonacci numbers' formula '''
    if x == 0 or x == 1:  # Remember that there are two base cases
        return 1
    else:
        return fib(x - 1) + fib (x - 2)
    

'''
Towers of Hanoi: imagine you have 3 towers where you can insert disks. You want to know how to reposition a stack of n disks of decreasing size from a tower to another, without ever putting a bigger disk on top of a smaller disk.
E.g. n = 4; fr (original tower) = T1; to (empty tower) = T2; spare (empty tower) = T3. The program above will print in order all of the instructions needed to solve the problem respecting the rules.
This is a hard to solve problem iteratively, but easily solvable recursively.
'''
def printMove(fr, to):
    print("move from " + str(fr) + " to " + str(to))

def Towers(n, fr, to, spare):
    if n == 1:
        printMove(fr, to)
    else:
        Towers(n-1, fr, spare, to)
        Towers(1, fr, to, spare)
        Towers(n-1, spare, to, fr)

        
'''
Number theory and modular arithmetic
'''
        
def gcdIter(a, b):
    ''' Iterative implementation of the Euclidean Algorithm
    a, b: positive integers
    returns: a positive integer, the greatest common divisor of a & b
    '''
    testv = min(a, b)
    while a % testv != 0 or b % testv != 0:
        testv -= 1
    return testv

def gcd(a, b):
    ''' Recursive implementation of the Euclidean Algorithm
    a, b: positive integers
    returns: a positive integer, the greatest common divisor of a & b
    '''
    assert a >= 0 and b >= 0, "Please input two positive integers."
    if b == 0:
        return a
    else:
        # print("a: ", a, ", b: ", b, ", a % b: ", a % b)  # Debug print
        return gcd(b, a % b)

def mod_mult(n1, n2, m):
    ''' Get x in the operation
        [x]_{m} = [n1]_{m} * [n2]_{m}  (modular multiplication) 
    '''
    return (n1 * n2) % m

def mult_inv(n, m):
    ''' Get the **multiplicative inverse** of number n, modulo m
        Note that the multiplicative inverse is unique (under m)
    '''
    for k in range(m):
        if mod_mult(n, k, m) == 1:
            mi = k
    return mi

def eulerset(m):
    ''' Get the set of the euler set for modulo m, that is
        (\mathbb{Z} / \equiv_{m})' =
        \{ [a]_{m} | 1 \leq a \leq 20 \land gcd(m, a) = 1 \}
    '''
    eset = []
    for n in range(m):
        if gcd(m, n) == 1:
            eset.append(n)
    return eset

def list_each_mult_inv(m):
    ''' Print a list of each of the multiplicative inverses
        for each element of the euler set, modulo m
        I.e. this lists the elements of Euler's phi on m
        WARNING: Don't use this for large m !
    '''
    euler_set = eulerset(m)
    for e in euler_set:
        mi = mult_inv(e, m)
        print("Element:", e, end="; ")
        print("multiplicative inverse:", mi)

def euler_phi_brute(m):
    ''' Returns the number of elements in the Euler set for m
        That is, the number of elements of the modular class m,
        that have a multiplicative inverse in that class
            The number should be the same as the number of elements
            printed in `list_each_mult_inv(m)`
            
        This is a brute-force algorithm, not using Euler's theorem
    '''
    mi_list = []
    euler_set = eulerset(m)
    for e in euler_set:
        mi = mult_inv(e, m)
        mi_list.append(mi)
    return len(mi_list)
