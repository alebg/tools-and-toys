#!/usr/bin/env bash

# 1. Basic floating point calculator for bash
# E.g. calc 1.5 + 1.5
# E.g. calc 3 / -7
# IMPORTANT: multiplier terms can't be separated by spaces (quote marks work)
# E.g. calc 3.3*-2  	# Works
# E.g. calc 3.3 * -2	# Doesn't work!
# E.g. calc "3.3 * -2"	# Also works

# 2. Basic mean calculators
# Arguments can be real numbers or basic arithmetic operations (without spaces)
# E.g. mean -10.5 -20.5 -30.5
# E.g. mean 10/-2 5*-1 10-15

# awk is the simplest and fastest of the options
    
# For 1. and 2. output can be redirected with | > >>
# Input cannot be passed with |, has to be passed with <<<
# E.g. calc <<< echo "2 + 2" > result.txt
# E.g. calc <<< echo "$(cat numbers.txt) + 0"  
	# Where numbers.txt contents is an operation, e.g. 2 + 2

######
# With awk
######

function calc {
    if [ $# -eq 1 ]; then
        awk "BEGIN{print $1}";
    else
        echo "Use: supply a single operation without spaces."
        echo "E.g. calc 3.3*-2"
    fi
}

function mean {
    local sum=0
    local mean=0

    for i in $@
    do
        sum="$(calc $sum + $i)"
    done

    mean="$(calc $sum / $#)"

    echo $mean
}

######
# With python
######

function pycalc {
    if [ $# -eq 1 ]; then
        python3 -c "print($1)"
    else
        echo "Use: supply a single operation without spaces."
        echo "E.g. pycalc 3.3*-2"
    fi
}

function pymean {
    local sum=0
    local mean=0

    for i in $@
    do
        sum="$(pycalc $sum + $i)"
    done

    mean="$(pycalc $sum / $#)"

    echo $mean
}


######
# With bccalc
######

function bccalc {
    bc -l <<< "$*"
}

function bcmean {
    local sum=0
    local mean=0

    for i in $@
    do
        sum="$(bccalc $sum + $i)"
    done

    mean="$(bccalc $sum / $#)"

    echo $mean
}
